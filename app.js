var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/routes-index');
var gameRouter = require('./routes/routes-game');

const app = express();
const session = require('express-session');
const flash = require('express-flash');

// Pertama, setting request body parser
// (Ingat! Body parser harus ditaruh paling atas)
app.use(express.urlencoded({ extended: false }));

// Kedua, setting session handler
app.use(
	session({
		secret: 'Buat ini jadi rahasia',
		resave: false,
		saveUninitialized: false
	})
);

// // Ketiga, setting passport
// // (sebelum router dan view engine)
// const passport = require('./lib/passport');
// app.use(passport.initialize());
// app.use(passport.session());

// app.use((req, res, next) => {
// 	res.locals.isAuthenticated = req.isAuthenticated();
// 	next();
// });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Keempat, setting flash
app.use(flash());

// Kelima, setting view engine
app.set('view engine', 'ejs');

// Keenam, setting router
app.use('/', indexRouter);
app.use('/game', gameRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
